--Reaper of the Hexibeasts
function c66600006.initial_effect(c)
	--xyz summon
	aux.AddXyzProcedure(c,aux.FilterBoolFunction(Card.IsAttribute,ATTRIBUTE_FIRE),6,2)
	c:EnableReviveLimit()
	--place into S/T zone
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(66600006,0))
	e1:SetType(EFFECT_TYPE_QUICK_O)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(c66600006.cost)
	e1:SetTarget(c66600006.target)
	e1:SetOperation(c66600006.operation)
	c:RegisterEffect(e1)
end

function c66600006.filter(c)
	return not c:IsType(TYPE_PENDULUM) and c:IsSetCard(0x666) and c:IsType(TYPE_MONSTER)
end

function c66600006.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end
function c66600006.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c66600006.filter,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,nil) end
end
function c66600006.operation(e,tp,eg,ep,ev,re,r,rp,chk)
	--local tc=g:GetFirst()
	if Duel.GetLocationCount(tp,LOCATION_SZONE)<=0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOFIELD)
	local g=Duel.SelectMatchingCard(tp,c66600006.filter,tp,LOCATION_DECK,0,1,1,nil)
	if g:GetCount()>0 then
		local tc=g:GetFirst()
		Duel.MoveToField(tc,tp,tp,LOCATION_SZONE,POS_FACEUP,true)
		local e1=Effect.CreateEffect(tc)
		e1:SetCode(EFFECT_CHANGE_TYPE)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fc0000)
		e1:SetValue(TYPE_SPELL+TYPE_CONTINUOUS)
		tc:RegisterEffect(e1)
		Duel.RaiseEvent(tc,66600006,e,0,tp,0,0)
	end
end