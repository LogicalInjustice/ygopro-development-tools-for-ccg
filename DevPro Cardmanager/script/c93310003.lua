--Summoner's Call
function c93310003.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(c93310003.condition)
	e1:SetTarget(c93310003.target)
	e1:SetOperation(c93310003.operation)
	c:RegisterEffect(e1)
end
function c93310003.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetFieldGroupCount(tp,LOCATION_MZONE,0)==0
end
function c93310003.filter(c,e,tp)
	return c:IsRace(RACE_SPELLCASTER) and c:GetLevel()==3 and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function c93310003.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsExistingMatchingCard(c93310003.filter,tp,LOCATION_DECK,0,1,nil,e,tp) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,1,tp,LOCATION_DECK)
end
function c93310003.filter2(c,e,tp)
	return c:IsRace(RACE_SPELLCASTER) and c:IsAbleToGrave()
end
function c93310003.operation(e,tp,eg,ep,ev,re,r,rp)
	if chk==0 then return  end
	local hg=Duel.GetFieldGroup(1-tp,LOCATION_DECK,0)
	if hg:GetCount()>0 and Duel.IsExistingMatchingCard(c93310003.filter2,1-tp,LOCATION_DECK,0,1,nil,e,tp) and Duel.SelectYesNo(1-tp,aux.Stringid(93310003,0)) then
		Duel.Hint(HINT_SELECTMSG,1-tp,aux.Stringid(93310003,1))
		local sg=Duel.SelectMatchingCard(1-tp,c93310003.filter2,tp,0,LOCATION_DECK,1,1,nil,e,tp)
		tc=sg:GetFirst()
		Duel.SendtoGrave(sg,REASON_EFFECT)
		if tc:IsLocation(LOCATION_GRAVE) and Duel.IsChainDisablable(0) then
			Duel.NegateEffect(0)
			return
		end
	end
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,c93310003.filter,tp,LOCATION_DECK,0,1,1,nil,e,tp)
	if g:GetCount()>0 then
		Duel.SpecialSummon(g,0,tp,tp,false,false,POS_FACEUP)
	end
	local tc=g:GetFirst()
	if tc then
		local e1=Effect.CreateEffect(tc)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_LEAVE_FIELD_REDIRECT)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetReset(RESET_EVENT+0x47e0000)
		e1:SetValue(LOCATION_REMOVED)
		tc:RegisterEffect(e1,true)
	end
end