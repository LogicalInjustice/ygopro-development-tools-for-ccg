--Hexibeast - Hellserpent
function c66600003.initial_effect(c)
local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(66600003,0))
	e1:SetCategory(CATEGORY_DESTROY+CATEGORY_DAMAGE)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetRange(LOCATION_SZONE)
	e1:SetCountLimit(1)
	e1:SetTarget(c66600003.damtg)
	e1:SetOperation(c66600003.damop)
	c:RegisterEffect(e1)
end
function c66600003.damtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetTargetPlayer(1-tp)
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,0)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,e:GetHandler(),1,0,0)
end
function c66600003.dfilter(c)
	return c:IsFaceup() and c:IsSetCard(0x666)
end
function c66600003.damop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local p=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER)
	local ct=Duel.GetMatchingGroupCount(c66600003.dfilter,tp,LOCATION_ONFIELD,0,nil)
if c:IsRelateToEffect(e) and Duel.Destroy(c,REASON_EFFECT)>0 then   
Duel.Damage(p,ct*400,REASON_EFFECT)
end
end