--Hexibeast Underworld
function c66600007.initial_effect(c)
local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	c:RegisterEffect(e1)
local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(66600007,0))
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_FZONE)
	e2:SetCountLimit(1)
	e2:SetCost(c66600007.cost)
	e2:SetTarget(c66600007.target)
	e2:SetOperation(c66600007.operation)
	c:RegisterEffect(e2)
--gain LP
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(66600007,0))
	e3:SetCategory(CATEGORY_RECOVER)
	e3:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_F)
	e3:SetCode(EVENT_DESTROY)
	e3:SetRange(LOCATION_SZONE)
	e3:SetCondition(c66600007.descondition)
	e3:SetTarget(c66600007.destarget)
	e3:SetOperation(c66600007.desoperation)
	c:RegisterEffect(e3)
end
function c66600007.filter(c)
	return c:IsSetCard(0x666) and c:IsType(TYPE_MONSTER) and not c:IsType(TYPE_PENDULUM)
end
function c66600007.cost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.CheckLPCost(tp,1000) end
	Duel.PayLPCost(tp,1000)
end
function c66600007.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(c66600007.filter,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,nil) and Duel.GetLocationCount(tp,LOCATION_SZONE)>0 end
end
function c66600007.operation(e,tp,eg,ep,ev,re,r,rp,chk)
	if Duel.GetLocationCount(tp,LOCATION_SZONE)<=0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOFIELD)
	local g=Duel.SelectMatchingCard(tp,c66600007.filter,tp,LOCATION_DECK+LOCATION_GRAVE,0,1,1,nil)
	if g:GetCount()>0 then
		local tc=g:GetFirst()
		Duel.MoveToField(tc,tp,tp,LOCATION_SZONE,POS_FACEUP,true)
		local e1=Effect.CreateEffect(tc)
		e1:SetCode(EFFECT_CHANGE_TYPE)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetReset(RESET_EVENT+0x1fc0000)
		e1:SetValue(TYPE_SPELL+TYPE_CONTINUOUS)
		tc:RegisterEffect(e1)
		Duel.RaiseEvent(tc,47408488,e,0,tp,0,0)
	end
end
function c66600007.desfilter(c,tp)
	return c:IsOnField() and c:IsFaceup() and c:IsControler(tp) and c:IsSetCard(0x666) and not c:IsType(TYPE_PENDULUM)
end
function c66600007.descondition(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(c66600007.desfilter,1,nil,tp)
end
function c66600007.destarget(e,tp,eg,ep,ev,re,r,rp,chk)
   if chk==0 then return e:GetHandler():IsRelateToEffect(e) end
end
function c66600007.desoperation(e,tp,eg,ep,ev,re,r,rp)
	if not e:GetHandler():IsRelateToEffect(e) then return end
	Duel.Recover(tp,500,REASON_EFFECT)
end